from flask import *
from bson.objectid import ObjectId
from flask_bootstrap import Bootstrap
from flask_pymongo import PyMongo

import bson

app= Flask('one-stop-shop')
app.config['SECRET_KEY'] = 'keykey'
app.config['MONGO_URI'] = "mongodb://localhost:27017/Onestopshop_db"
Bootstrap(app)
mongo = PyMongo(app)
@app.route('/', methods=['GET', 'POST'])
def main():
    if request.method == 'GET':
        return render_template('mainpage.html')

@app.route('/addproduct', methods=['GET', 'POST'])
def add():
    if request.method == 'GET':
        return render_template('addproduct.html')
    elif request.method== 'POST':
        doc = {}
        for item in request.form:
            doc[item] = request.form[item]
        mongo.db.products.insert_one(doc)
        return redirect('/')
@app.route('/buyproduct', methods=['GET', 'POST'])
def buy():
    if request.method == 'GET':
        session['cart-items'] = {}
        found_products = mongo.db.products.find
        return render_template('buyproduct.html', products=found_products)
    elif request.method == 'POST':
        doc = {}
        for item in request.form:
            if int(request.form[item]) != 0:
                doc[item] = request.form[item]
        session['cart-items'] = doc
        return redirect('/checkout')
@app.route('/checkout')
def checkout():
    total=0
    cart_items = []
    stored_info = session['cart-items']
    for ID in stored_info:
        found_item = mongo.db.items.find_one({'_id', ObjectId(ID)})
        found_item['bought'] = stored_info[ID]
        found_item['item-total'] = int(found_item['price']) * int(found_item['bought'])
        cart_items.append(found_item)
        total += found_item['item-total']
    return render_template(checkout.html, products= cart_items, total=total)
app.run(debug=True)

